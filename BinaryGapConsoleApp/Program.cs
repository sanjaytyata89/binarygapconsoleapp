﻿using System;

namespace BinaryGapConsoleApp

{
    class Program
    {
        static void Main(string[] args)
        {
            int result = findSolutionFor(15);
            Console.Write(result);
            Console.ReadKey();
        }
        private static int findSolutionFor(int N)
        {
            string number = Convert.ToString(N, 2);
            int count = 0;
            int maxCount = 0;

            for (int i = 0; i < number.Length; i++)
            {
                if (number[i] == '0')
                {
                    count = count + 1;
                }
                else
                {
                    maxCount = Math.Max(maxCount, count);
                    count = 0;
                }
            }
            return maxCount;
        }

    }
}